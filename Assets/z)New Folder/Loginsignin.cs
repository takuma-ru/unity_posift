﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using NCMB;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

//private InputField UserName;
//private InputField PassWord;

public class Loginsignin : MonoBehaviour
{
	public InputField UserName;
	public InputField PassWord;

    public Animator UIAnim;
    public Animator UIAnim_effect;
    public Text Point;

    public GameObject User;
    //public GameObject SuccessText;
    Text uname;
    Text ulevel;
    Text uexp;
    public Text Error;

    public static string UserNametext;
    public static string level;
    public static bool online = false;
    // Use this for initialization
    void Start ()
	{
        uname = User.transform.Find("usernametext").GetComponent<Text>();
        ulevel = User.transform.Find("userLeveltext").GetComponent<Text>();
        uexp = User.transform.Find("userExptext").GetComponent<Text>();

        if (ResultController.login)
        {
            UserInput();
            //UIAnim.speed = -1;
        }
    }

	void Update ()
	{
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("StartScene");
        }
	}

    public void save(string name, int max, int level, int exp)
    {
        // 保存先クラスを作成
        NCMBObject obj = new NCMBObject("UserData");
        // 値を設定
        obj["name"] = name;
        obj["MaxPoint"] = max;
        obj["Level"] = level;
        obj["Exp"] = exp;
        // 保存を実施
        obj.SaveAsync((NCMBException e) => {
            if (e != null)
            {
                // 保存に失敗した場合の処理
                Debug.Log("保存に失敗しました。エラーコード:" + e.ErrorCode);
            }
            else
            {
                // 保存に成功した場合の処理
                Debug.Log("保存に成功しました。objectId:" + obj.ObjectId);
            }
        });
    }


    public void UserInput()
    {
        NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject>("UserData");
        query.WhereEqualTo("name", Loginsignin.UserNametext);
        query.FindAsync((List<NCMBObject> objList, NCMBException e) => {

            //検索成功したら
            if (e == null)
            {
                int score = System.Convert.ToInt32(objList[0]["MaxPoint"]);
                Debug.Log(score);
                Debug.Log("検索成功！");

                uname.text = (objList[0]["name"]).ToString();
                Point.text = (objList[0]["MaxPoint"]).ToString();
                ulevel.text = "Lv." + objList[0]["Level"];
                uexp.text = "TotalExp:" + objList[0]["Exp"];
            }
            level = ulevel.text;
            online = true;
        });
    }

    public void Login ()
	{
        UIAnim.SetBool("verificationErrorBool", false);
        UIAnim.SetBool("verificationBool", true);
        UIAnim_effect.SetBool("verificationErrorBool", false);
        UIAnim_effect.SetBool("verificationBool", true);

        print (UserName.text);
		print (PassWord.text);

        UserNametext = UserName.text;

        //NCMBUserのインスタンス作成
        NCMBUser user = new NCMBUser ();

        // ユーザー名とパスワードでログイン
        NCMBUser.LogInAsync (UserName.text, PassWord.text, (NCMBException e) => {
			if (e != null) {
				UnityEngine.Debug.Log ("ログインに失敗: " + e.ErrorMessage);
                Error.color = new Color(1, 0, 0);
                UIAnim.SetBool("verificationBool", false);
                UIAnim.SetBool("verificationErrorBool", true);
                UIAnim_effect.SetBool("verificationBool", false);
                UIAnim_effect.SetBool("verificationErrorBool", true);
                Error.text = e.ErrorMessage;
            } else {
				UnityEngine.Debug.Log ("ログインに成功！");
                uname.text = UserNametext;
                Error.text = null;
                UIAnim.SetBool("NextBool", true);
                UIAnim.SetBool("NextbackBool", false);
                UIAnim.SetBool("BackBool", true);
                UIAnim.SetBool("verificationErrorBool", false);
                UIAnim.SetBool("verificationBool", false);
                UIAnim_effect.SetBool("NextBool", true);
                UIAnim_effect.SetBool("NextbackBool", false);
                UIAnim_effect.SetBool("BackBool", true);
                UIAnim_effect.SetBool("verificationErrorBool", false);
                UIAnim_effect.SetBool("verificationBool", false);
                UserName.text = null;
                PassWord.text = null;
                UserInput();
            }
		});
    }

    public void Signin ()
	{
        UIAnim.SetBool("verificationErrorBool", false);
        UIAnim.SetBool("verificationBool", true);
        UIAnim_effect.SetBool("verificationErrorBool", false);
        UIAnim_effect.SetBool("verificationBool", true);

        print (UserName.text);
		print (PassWord.text);

		//NCMBUserのインスタンス作成
		NCMBUser user = new NCMBUser ();
		//ユーザ名とパスワードの設定
		user.UserName = UserName.text;
		user.Password = PassWord.text;

        UserNametext = UserName.text;

        //会員登録を行う
        user.SignUpAsync ((NCMBException e) => { 
			if (e != null) {
				UnityEngine.Debug.Log ("新規登録に失敗: " + e.ErrorMessage);
                Error.color = new Color(1, 0, 0);
                UIAnim.SetBool("verificationBool", false);
                UIAnim.SetBool("verificationErrorBool", true);
                UIAnim_effect.SetBool("verificationBool", false);
                UIAnim_effect.SetBool("verificationErrorBool", true);
                Error.text = e.ErrorMessage;
            } else {
				UnityEngine.Debug.Log ("新規登録に成功");
                uname.text = UserNametext;
                Error.text = null;
                UIAnim.SetBool("NextBool", true);
                UIAnim.SetBool("NextbackBool", false);
                UIAnim.SetBool("BackBool", true);
                UIAnim.SetBool("verificationErrorBool", false);
                UIAnim.SetBool("verificationBool", false);
                UIAnim_effect.SetBool("NextBool", true);
                UIAnim_effect.SetBool("NextbackBool", false);
                UIAnim_effect.SetBool("BackBool", true);
                UIAnim_effect.SetBool("verificationErrorBool", false);
                UIAnim_effect.SetBool("verificationBool", false);
                UserName.text = null;
                PassWord.text = null;
                save(user.UserName, 0, 1, 0);
            }
            UserInput();
        });
    }

    void Awake()
    {
        Application.targetFrameRate = 60;
    }

}
