﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NCMB;

public class RankingController : MonoBehaviour
{
    public GameObject rankingscreen;
    Animator rankingAnim;

    public GameObject settingscreen;
    Animator settingAnim;

    bool active = false;

    public void OnClickRanking()
    {
        if (!active)
        {
            rankingAnim.SetBool("CloseBool", false);
            rankingAnim.SetBool("OpenBool", true);
            active = true;
        }
    }

    public void ExitRanking()
    {
        rankingAnim.SetBool("OpenBool", false);
        rankingAnim.SetBool("CloseBool", true);
        active = false;
    }

    public void OnClickSetting()
    {
        if (!active)
        {
            settingAnim.SetBool("CloseBool", false);
            settingAnim.SetBool("OpenBool", true);
            active = true;
        }
    }

    public void ExitSetting()
    {
        settingAnim.SetBool("OpenBool", false);
        settingAnim.SetBool("CloseBool", true);
        active = false;
    }

    void Start()
    {
        rankingAnim = rankingscreen.GetComponent<Animator>();
        rankingAnim.speed = 3;

        settingAnim = settingscreen.GetComponent<Animator>();
        settingAnim.speed = 3;

        // データストアの「HighScore」クラスから検索
        /*NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject>("UserData");
        query.OrderByDescending("MaxPoint");
        query.Limit = 5;
        query.FindAsync((List<NCMBObject> objList, NCMBException e) => {

            if (e != null)
            {
                //検索失敗時の処理
            }
            else
            {
                //検索成功時の処理
                List<HighScore> list = new List<HighScore>();
                // 取得したレコードをHighScoreクラスとして保存
                foreach (NCMBObject obj in objList)
                {
                    int s = System.Convert.ToInt32(obj["Score"]);
                    string n = System.Convert.ToString(obj["Name"]);
                    list.Add(new HighScore(s, n));
                }
                topRankers = list;
            }
        });*/
    }

    void Update()
    {
        
    }
}
