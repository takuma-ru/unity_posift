﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NCMB;
using UnityEngine.SceneManagement;

public class LoginSigninController : MonoBehaviour
{
    public GameObject Error;
    public Text ErrorLogin;
    Animator Anim;
    public Animator UIAnim_effect;

    public RuntimeAnimatorController Canvas2;

    public Animation userAnim;

    public static bool skip_b;

    public void OnClick()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void OnClickback()
    {
        NCMBUser.LogOutAsync((NCMBException e) => {
            if (e != null)
            {
                UnityEngine.Debug.Log("ログアウトに失敗: " + e.ErrorMessage);
                Error.SetActive(true);
                Error.GetComponent<Text>().color = new Color(255 / 255f, 0 / 255f, 0 / 255f, 255);
                Error.GetComponent<Text>().text = e.ErrorMessage;
            }
            else
            {
                UnityEngine.Debug.Log("ログアウトに成功");
                Anim.SetBool("BackBool", true);
                Anim.SetBool("NextBool", false);
                Anim.SetBool("NextbackBool", true);
                Anim.SetBool("verificationErrorBool", false);
                Anim.SetBool("verificationBool", false);
                UIAnim_effect.SetBool("BackBool", true);
                UIAnim_effect.SetBool("NextBool", false);
                UIAnim_effect.SetBool("NextbackBool", true);
                UIAnim_effect.SetBool("verificationErrorBool", false);
                UIAnim_effect.SetBool("verificationBool", false);
                Error.SetActive(false);
                Error.GetComponent<Text>().text = null;
                ErrorLogin.text = null;
            }
        });
    }

    void Start()
    {
        UIAnim_effect.speed = 1.5f;
        Anim = this.GetComponent<Animator>();
        if (skip_b)
        {
            Anim.runtimeAnimatorController = Canvas2;
            UIAnim_effect.runtimeAnimatorController = Canvas2;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Anim.speed = 1.5f;
    }
}
