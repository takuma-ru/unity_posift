﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartController : MonoBehaviour
{
    Text Text_load;
    public GameObject End;
    public GameObject End_effect;

    bool nowload_b = false;
    public static bool end_b = false;

    public void Quit()
    {
        if (endtrue.end_bs)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
            UnityEngine.Application.Quit();
#endif
        }
    }

    public void On()
    {
        End.SetActive(true);
        End_effect.SetActive(true);
        end_b = true;
    }

    public void Off()
    {
        End.SetActive(false);
        End_effect.SetActive(false);
        end_b = false;
    }

    void Start()
    {
        Text_load = GameObject.Find("Canvas_noeffect").transform.Find("Loadtext").GetComponent<Text>();
        LoginSigninController.skip_b = false;
        Off();
    }

    // Update is called once per frame
    void Update()
    {
        if (end_b && !nowload_b)
        {
            if (Input.GetKeyUp(KeyCode.Return))
            {
                Quit();
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                Off();
            }
        }
        else if(!nowload_b)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                On();
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Text_load.text = "Now Loading";
                SceneManager.LoadScene("LoginScene");
                GameObject.Find("Canvas_noeffect").transform.Find("Loadtext").gameObject.SetActive(true);
                //nowload_b = true;
            }
        }else if (nowload_b)
        {
            StartCoroutine("LoadScene");
        }
    }

    IEnumerator LoadScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("LoginScene");
        asyncLoad.allowSceneActivation = false;
        while (true)
        {
            yield return null;
            // 読み込み完了したら
            if (asyncLoad.progress >= 0.9f)
            {
                // シーン読み込み
                asyncLoad.allowSceneActivation = true;
                break;
            }
        }

    }
}