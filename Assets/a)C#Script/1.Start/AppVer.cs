﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AppVer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("V" + Application.version);
        this.GetComponent<TextMeshProUGUI>().text = "V" + Application.version;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
