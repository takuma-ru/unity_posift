﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingController_StoLS : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("LoadScene");
    }

    IEnumerator LoadScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("LoginScene");
        asyncLoad.allowSceneActivation = false;
        while (true)
        {
            yield return null;
            // 読み込み完了したら
            if (asyncLoad.progress >= 0.9f)
            {
                // シーン読み込み
                asyncLoad.allowSceneActivation = true;
                break;
            }
        }

    }
}
