﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoController : MonoBehaviour
{
    public bool loading_b;


    void Start()
    {
        loading_b = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (loading_b)
        {
            SceneManager.LoadScene("LDScene_LtoS");
        }
    }
}
