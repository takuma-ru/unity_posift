﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OnTriggerController : MonoBehaviour
{

    public Material BLUE;
    public Material Nomal;

    GameObject Pttext;
    public GameObject Generator;
    public GameObject PointOb;
    public GameObject PointTx;
    public GameObject PointCr;
    public GameObject CircleAnim;
    public GameObject Circle_rsAnim;

    TypefaceAnimator PointSc;

    GeneratorScript script;
    Renderer mate;
    public Text tpoint;
    Image Circle;

    int num = 0;
    string color;
    private float presskeyFrames = 0;
    // 長押し判定の閾値（フレーム数）
    private int thresholdLong = 60;

    void OnTriggerStay(Collider other)
    {
        Circle = transform.Find("Canvas").transform.Find("Circle").GetComponent<Image>();
        TextMeshPro Point = transform.Find("Canvas").Find("Pointtext").GetComponent<TextMeshPro>();

        presskeyFrames += Time.deltaTime;

        int point = int.Parse(Point.text);
        thresholdLong = point;
        Circle.fillAmount -= (1.0f / point * Time.deltaTime);

        if (thresholdLong <= presskeyFrames || point < 0)
        {
            if (other.CompareTag("Player_BLUE") && color != "blue")
            {
                mate.material = BLUE;
                color = "blue";
                Point.color = new Color(35 / 255f, 191 / 255f, 191 / 255f, 255);
                Circle.color = new Color(35 / 255f, 191 / 255f, 191 / 255f, 255);
                PointSc.enabled = false;
                PointSc.enabled = true;
                num = int.Parse(tpoint.text);
                if (GeneratorScript.Inversionworld_b)
                {
                    tpoint.text = (num + (point * 2)).ToString();
                    script.totalpoint += (point * 2);
                }
                else
                {
                    tpoint.text = (num + point).ToString();
                    script.totalpoint += point;
                }
                script.totaltrout++;
                CircleAnim.SetActive(true);
                Circle_rsAnim.SetActive(true);
            }
        }
        if(color == "blue" || point < 0)
        {
            Circle.fillAmount += 0.1f;
        }
    }

    void OnTriggerExit(Collider other)
    {
        presskeyFrames = 0;
        Circle.fillAmount = 1.0f;
    }

    void Start()
    {
        mate = GetComponent<Renderer>();
        script = Generator.GetComponent<GeneratorScript>();
        PointOb = GameObject.Find("Canvas").transform.Find("Point").transform.Find("Point").gameObject;
        PointSc = PointOb.GetComponent<TypefaceAnimator>();
        CircleAnim.SetActive(false);
        Circle_rsAnim.SetActive(false);
        //tpoint = GameObject.Find("Canvas").transform.Find("Point").transform.Find("Point").GetComponent<Text>();
    }

    void Update()
    {

    }
}
