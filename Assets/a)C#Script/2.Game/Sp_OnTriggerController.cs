﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Sp_OnTriggerController : MonoBehaviour
{

    public Material BLUE;
    public Material ccubeBLUE;
    public Material pcubeBLUE;
    public Material Nomal;

    public GameObject Generator;
    public GameObject PointOb;
    public GameObject CircleAnim;
    public GameObject Circle_rsAnim;
    public GameObject Judgment;
    public GameObject AllGrid;

    public Animator AllGridAnim;

    TypefaceAnimator PointSc;

    GeneratorScript script;
    Renderer mate;
    public Text tpoint;
    Image Circle;

    int num = 0;
    string color;

    public static bool pause = false;

    void OnTriggerStay(Collider other)
    {
        if (Input.GetMouseButtonDown(0))
        {
            AllGrid.GetComponent<Animator>().enabled = false;
            AllGrid.GetComponent<Animator>().enabled = true;
            if (other.CompareTag("Player_BLUE") && color != "blue")
            {
                GeneratorScript.Inversionworld_b = true;

                num = int.Parse(tpoint.text);
                tpoint.text = (num - 5).ToString();
                script.totalpoint -= 5;

                mate.material = BLUE;
                transform.Find("Cube").transform.Find("Center").GetComponent<Renderer>().material = pcubeBLUE;
                transform.Find("Cube").transform.Find("UpRight_f").GetComponent<Renderer>().material = ccubeBLUE;
                transform.Find("Cube").transform.Find("DownRight_f").GetComponent<Renderer>().material = ccubeBLUE;
                transform.Find("Cube").transform.Find("UpLeft_f").GetComponent<Renderer>().material = ccubeBLUE;
                transform.Find("Cube").transform.Find("DownLeft_f").GetComponent<Renderer>().material = ccubeBLUE;
                transform.Find("Cube").transform.Find("UpRight").GetComponent<Renderer>().material = ccubeBLUE;
                transform.Find("Cube").transform.Find("DownRight").GetComponent<Renderer>().material = ccubeBLUE;
                transform.Find("Cube").transform.Find("UpLeft").GetComponent<Renderer>().material = ccubeBLUE;
                transform.Find("Cube").transform.Find("DownLeft").GetComponent<Renderer>().material = ccubeBLUE;
                color = "blue";
                CircleAnim.SetActive(true);
                Circle_rsAnim.SetActive(true);
                //pause = true;
                //AllGridAnim.GetComponent<Animation>()["AllGridAnim"].normalizedTime = 0.0f;
                //Judgment.SetActive(true);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {

    }

    void Start()
    {
        mate = GetComponent<Renderer>();
        script = Generator.GetComponent<GeneratorScript>();
        PointOb = GameObject.Find("Canvas").transform.Find("Point").transform.Find("Point").gameObject;
        PointSc = PointOb.GetComponent<TypefaceAnimator>();
        CircleAnim.SetActive(false);
        Circle_rsAnim.SetActive(false);
    }

    void Update()
    {
        /*if (JudgmentController.Judgmentactive_b)
        {
            mate.material = BLUE;
            transform.Find("Cube").transform.Find("Center").GetComponent<Renderer>().material = pcubeBLUE;
            transform.Find("Cube").transform.Find("UpRight_f").GetComponent<Renderer>().material = ccubeBLUE;
            transform.Find("Cube").transform.Find("DownRight_f").GetComponent<Renderer>().material = ccubeBLUE;
            transform.Find("Cube").transform.Find("UpLeft_f").GetComponent<Renderer>().material = ccubeBLUE;
            transform.Find("Cube").transform.Find("DownLeft_f").GetComponent<Renderer>().material = ccubeBLUE;
            transform.Find("Cube").transform.Find("UpRight").GetComponent<Renderer>().material = ccubeBLUE;
            transform.Find("Cube").transform.Find("DownRight").GetComponent<Renderer>().material = ccubeBLUE;
            transform.Find("Cube").transform.Find("UpLeft").GetComponent<Renderer>().material = ccubeBLUE;
            transform.Find("Cube").transform.Find("DownLeft").GetComponent<Renderer>().material = ccubeBLUE;
            color = "blue";
            PointSc.enabled = false;
            PointSc.enabled = true;
            num = int.Parse(tpoint.text);
            tpoint.text = (num + 12).ToString();
            script.totaltrout++;
            script.totalpoint += 12;
            CircleAnim.SetActive(true);
            JudgmentController.Judgmentactive_b = false;
        }*/
    }
}

