﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NCMB;
using UnityEngine.SceneManagement;

public class ResultController : MonoBehaviour
{  

    public GameObject Generator;
    GameObject Record;
    GameObject user;
    GameObject breakdown;

    Text Point;
    Text Attack;
    
    GeneratorScript script;

    int exp;

    public static bool login = false;
    public static bool active = false;
    public void fetch()
    {
        NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject>("UserData");
        query.WhereEqualTo("name", Loginsignin.UserNametext);
        query.FindAsync((List<NCMBObject> objList, NCMBException e) => {

            //検索成功したら
            if (e == null)
            {
                int score = System.Convert.ToInt32(objList[0]["MaxPoint"]);
                Debug.Log(score);
                Debug.Log("検索成功！");

                //Exp
                objList[0]["Exp"] = System.Convert.ToInt32(objList[0]["Exp"]) + exp;
                user.transform.Find("EXPtext").GetComponent<Text>().text = "TotalExp." + objList[0]["Exp"];
                objList[0].Save();

                objList[0]["Level"] = 1 + Mathf.FloorToInt(System.Convert.ToInt32(objList[0]["Exp"]) / 95);
                user.transform.Find("NEXTtext").GetComponent<Text>().text = "next." + (95 - (System.Convert.ToInt32(objList[0]["Exp"]) - (System.Convert.ToInt32(objList[0]["Level"])-1) * 95));
                user.transform.Find("userLeveltext").GetComponent<Text>().text = "Lv." + objList[0]["Level"];
                objList[0].Save();

                //最高得点更新
                if (exp > score)
                {
                    Record.SetActive(true);
                    objList[0]["MaxPoint"] = exp;
                    objList[0].Save();
                }

                Debug.Log("MaxPoint:" + objList[0]["MaxPoint"] + ", exp:" + objList[0]["Exp"] + ", Lv:" + objList[0]["Level"]);
            }
        });
    }

    void Start()
    {
        active = true;
        script = Generator.GetComponent<GeneratorScript>();
        Point = transform.Find("yourpoint").transform.Find("totalpoint").gameObject.GetComponent<Text>();
        Attack = transform.Find("attacknumber").transform.Find("numbertext").GetComponent<Text>();
        Record = transform.Find("yourpoint").transform.Find("NewRecordtext").gameObject;
        user = transform.Find("Resultuser").gameObject;
        breakdown = user.transform.Find("Breakdown").gameObject;

        if (GeneratorScript.online)
        {
            fetch();
        }
    }

    public void OnClick()
    {
        SceneManager.LoadScene("LoginScene");
        LoginSigninController.skip_b = true;
        login = true;
    } 
    void Update()
    {
        exp = script.totalpoint + script.totaltrout - EnemyNavigationController.Attack * 2;

        transform.Find("yourpoint").transform.Find("totalpoint").GetComponent<Text>().text = (script.totalpoint + script.totaltrout - EnemyNavigationController.Attack * 2).ToString();
        transform.Find("trout").transform.Find("trouttext").GetComponent<Text>().text = (script.totaltrout).ToString() + "/25";
        transform.Find("point").transform.Find("pointtext").GetComponent<Text>().text = (script.totalpoint).ToString();
        transform.Find("Acquisitionrate").transform.Find("percenttext").GetComponent<Text>().text = (script.rate).ToString() + "%";
        Attack.text = (EnemyNavigationController.Attack).ToString();

        breakdown.transform.Find("pointnumber").GetComponent<Text>().text = (script.totalpoint).ToString();
        breakdown.transform.Find("troutnumber").GetComponent<Text>().text = (script.totaltrout).ToString();
        breakdown.transform.Find("attacknumber").GetComponent<Text>().text = "-" + (EnemyNavigationController.Attack).ToString() + "×2";
        breakdown.transform.Find("totalnumber").GetComponent<Text>().text = (script.totalpoint + script.totaltrout - EnemyNavigationController.Attack * 2).ToString();

    }
}
