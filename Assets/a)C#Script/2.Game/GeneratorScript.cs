﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Linq;
using NCMB;

public class GeneratorScript : MonoBehaviour
{
    public int totaltrout = 0;
    public int totalpoint = 0;
    public int attacksenemy = 0;
    public float rate = 0.0f;

    public Text username;
    public Text rusername;
    public Text level;
    public Text onlinetext;
    public Text ronlinetext;
    //public Text counttext;
    public TextMeshProUGUI readytext;

    public GameObject player;
    public GameObject enemy;
    public GameObject time;
    public GameObject countOb;
    public GameObject pauseOb;
    public GameObject Pausetext;

    float currentTime = 5;

    bool CountDown_b = true;

    public static bool online;
    public static bool pause_b = false;
    public static bool Inversionworld_b = false;

    public void Pause()
    {
        if (pause_b && ResultController.active == false)
        {
            Cursor.lockState = CursorLockMode.None;
            player.GetComponent<PlayerController>().enabled = false;
            player.GetComponent<CapsuleCollider>().enabled = false;
            player.GetComponent<Animator>().SetFloat("MovingSpeed", 0.0f);
            enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
            enemy.GetComponent<EnemyNavigationController>().enabled = false;
            //enemy.GetComponent<Animator>().enabled = false;
            time.GetComponent<TimelimitController>().enabled = false;
        }
        else if (ResultController.active == false)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Confined;
            player.GetComponent<PlayerController>().enabled = true;
            player.GetComponent<CapsuleCollider>().enabled = true;
            player.GetComponent<Animator>().SetFloat("MovingSpeed", 1.0f);
            enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
            enemy.GetComponent<EnemyNavigationController>().enabled = true;
            //enemy.GetComponent<Animator>().enabled = true;
            time.GetComponent<TimelimitController>().enabled = true;
        }
    }

    public void CountDown()
    {
        if (currentTime >= 2)
        {

        }
        else if (currentTime >= 1)
        {
            readytext.text = "<color=#2dbfbf>GO</color>";
            countOb.GetComponent<Animator>().SetBool("CloseBool", true);
            countOb.transform.Find("readygo").GetComponent<Animator>().SetBool("CloseBool", true);
            player.GetComponent<PlayerController>().enabled = true;
            enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
            enemy.GetComponent<EnemyNavigationController>().enabled = true;
            //enemy.GetComponent<Animator>().enabled = true;
            time.GetComponent<TimelimitController>().enabled = true;
        }
        else
        {
            countOb.gameObject.SetActive(false);
            CountDown_b = false;
        }
        currentTime -= Time.deltaTime;
    }

    void Start()
    {
        username.text = Loginsignin.UserNametext;
        rusername.text = Loginsignin.UserNametext;
        level.text = Loginsignin.level;
        online = Loginsignin.online;

        if (online)
        {
            onlinetext.text = "-User authenticated-";
            ronlinetext.text = "-User authenticated-";
            onlinetext.color = new Color(0 / 255f, 255 / 255f, 157 / 255f, 255);
            ronlinetext.color = new Color(0 / 255f, 255 / 255f, 157 / 255f, 255);
        }
        else
        {
            onlinetext.text = "-Unauthenticated user-";
            ronlinetext.text = "-Unauthenticated user-";
            onlinetext.color = new Color(255 / 255f, 99 / 255f, 0 / 255f, 255);
            ronlinetext.color = new Color(255 / 255f, 99 / 255f, 0 / 255f, 255);
        }


        //シャッフルする配列
        int[] ary = new int[] { -3, -2, -2, -1, -1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5 };

        //シャッフルする
        int[] ary2 = ary.OrderBy(i => Guid.NewGuid()).ToArray();

        for (int i = 0; i <= 23; i++){
            GameObject Grid = GameObject.Find("AllGrid").transform.Find("Grid_" + i).gameObject;
            
            TextMeshPro Point = Grid.transform.Find("Canvas").transform.Find("Pointtext").GetComponent<TextMeshPro>();
            
            int Pt = 0;
            while (Pt == 0)
            {
                Pt = ary2[i];
            }
            
            Point.text = (Pt).ToString();
        }

        pauseOb.SetActive(false);
        Pausetext.SetActive(false);
    }

    void Update()
    {
        rate = totalpoint * 1.5625f;

        if (CountDown_b)
        {
            CountDown();
        }

        if (CountDown_b == false)
        {
            if (Sp_OnTriggerController.pause)
            {
                pause_b = true;
                Pause();
                Cursor.visible = false;
                Debug.Log(pause_b);
            }
            else if (Input.GetKeyUp(KeyCode.Escape))
            {
                if (pause_b) { 
                    Pausetext.SetActive(false);
                    pauseOb.SetActive(false); 
                    pause_b = false; 
                }
                else 
                {
                    Pausetext.SetActive(true);
                    pauseOb.SetActive(true);
                    pause_b = true;
                    Cursor.visible = true;
                }
                //Debug.Log(pause_b);
                Pause();
            }else if (JudgmentController.Judgmentactive_b)
            {
                pause_b = false;
                Pause();
                //JudgmentController.Judgmentactive_b = false;
            }
        }
        //１試合した後、ゲームを終了せずにもう一度試合を始めるとPauseが出来なくなる❕
    }

    void Awake()
    {
        ResultController.active = false;
        pause_b = false;
        Application.targetFrameRate = 60;
    }
}
