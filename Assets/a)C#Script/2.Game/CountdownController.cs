﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using TMPro;

public class CountdownController : MonoBehaviour
{

    public Text counttext;
    //public Text readytext;
    public TextMeshProUGUI readytext;

    //public Image circle;

    public GameObject player;
    public GameObject enemy;
    public GameObject time;

    float currentTime = 5;

    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        if (currentTime >= 2)
        {
            //counttext.text = "1";
            //this.GetComponent<Animator>().SetBool("CloseBool", true);
        }
        else if (currentTime >= 1)
        {
            //counttext.text = "0";
            readytext.text = "<color=#2dbfbf>GO</color>";
            this.GetComponent<Animator>().SetBool("CloseBool", true);
            transform.Find("readygo").GetComponent<Animator>().SetBool("CloseBool", true);
            player.GetComponent<PlayerController>().enabled = true;
            enemy.GetComponent<NavMeshAgent>().enabled = true;
            enemy.GetComponent<EnemyNavigationController>().enabled = true;
            //enemy.GetComponent<Animator>().enabled = true;
            time.GetComponent<TimelimitController>().enabled = true;
        }
        else
        {
            gameObject.SetActive(false);
        }
        currentTime -= Time.deltaTime;
    }
}
