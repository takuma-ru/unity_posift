﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyNavigationController : MonoBehaviour
{
    public GameObject target;
    public GameObject canvas;
    public GameObject Player;

    public static int Attack;

    NavMeshAgent agent;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player_BLUE"))
        {
            //Debug.Log("Attack!");
            Attack++;
            Player.transform.position = new Vector3(0, 1, -15); 
        }
    }

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        Attack = 0;
    }

    void Update()
    {
        agent.destination = target.transform.position;
        // agentにNavMeshAgentのインスタンスが設定されていて、
        // posに目的地が入っているとします
        //agent.SetDestination(agent.destination);
    }
}
