﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JudgmentController : MonoBehaviour
{
    GameObject Judbar_red;
    GameObject Judbar_blue;
    GameObject JudCircle_red;
    GameObject JudCircle_blue;

    int finish;
    public static int Rank_r;
    public static int Rank_b;

    float rotr_z;
    float rotb_z;
    float bar_r = 0;
    float bar_b = 180;

    bool startr_b = false;
    bool startb_b = false;
    bool starta_b = false;

    public static bool Judgmentactive_b = false;

    void Start()
    {
        Judbar_red = transform.Find("Judmentbar_red").gameObject;
        Judbar_blue = transform.Find("Judmentbar_blue").gameObject;
        JudCircle_red = transform.Find("JudgmentCircle_red_s").gameObject;
        JudCircle_blue = transform.Find("JudgmentCircle_blue_s").gameObject;


        rotr_z = Random.Range(20, 350);
        rotb_z = Random.Range(20, 350);
        while (rotr_z == Judbar_red.transform.eulerAngles.z && rotr_z == Judbar_blue.transform.eulerAngles.z) 
        {
            rotr_z = Random.Range(20, 350);
        }
        JudCircle_red.transform.Rotate(0, 0, rotr_z);
        while ((rotb_z >= rotr_z && rotb_z <= rotr_z + 18) || (rotb_z <= rotr_z && rotb_z >= rotr_z - 18) || (rotb_z == Judbar_red.transform.eulerAngles.z && rotb_z == Judbar_blue.transform.eulerAngles.z))
        {
            rotb_z = Random.Range(20, 350);
        }   
        JudCircle_blue.transform.Rotate(0, 0, rotb_z);

    }

    void Update()
    {   
        if (Input.GetKeyDown(KeyCode.E)) startb_b = false;
        if (Input.GetKeyDown(KeyCode.Q)) startr_b = false;
        if (Input.GetKeyUp(KeyCode.E) && starta_b == false)
        {
            startr_b = true; startb_b = true; starta_b = true;
        }

        if (startr_b)
        {
            Judbar_red.transform.Rotate(new Vector3(0, 0, 180 * Time.deltaTime));//180
        }
        if (startb_b)
        {
            Judbar_blue.transform.Rotate(new Vector3(0, 0, -300 * Time.deltaTime));//-300
        }

        if (starta_b)
        {
            if(startr_b == false)
            {
                bar_r = Judbar_red.transform.eulerAngles.z;
                if ((-bar_r >= rotr_z + 5 && -bar_r <= rotr_z + 12) || (bar_r <= rotr_z - 5 && bar_r >= rotr_z - 12))
                {
                    //Debug.Log(bar_r + "::" + rotr_z);
                }
            }
            if (startb_b == false)
            {
                bar_b = Judbar_blue.transform.eulerAngles.z;
                if ((-bar_r >= rotr_z + 8 && -bar_r <= rotr_z + 20.5) || (bar_r <= rotr_z - 8 && bar_r >= rotr_z - 20.5))
                {
                    //Debug.Log(bar_r + "::" + rotr_z);
                }
            }
            if(startr_b == false && startb_b == false)
            {
                Judgmentactive_b = true;
                Sp_OnTriggerController.pause = false;
                this.gameObject.SetActive(false);
                //Debug.Log(Sp_OnTriggerController.pause);
            }
        }

    }
}
