﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class TimelimitController : MonoBehaviour
{
    public Text timerText;
    public GameObject player;
    public GameObject enemy;
    public GameObject Point;
    public GameObject Result;

    float totalTime;
    float thisTime;
	int seconds;
 
	// Use this for initialization
	void Start () {
        thisTime = int.Parse(timerText.text);
        totalTime = thisTime;
    }
 
	// Update is called once per frame
	void Update () {
		if(thisTime >= 0 ){
			thisTime -= Time.deltaTime;
			seconds = (int)thisTime;
			timerText.text= (seconds).ToString();
        	this.GetComponent<Image>().fillAmount -= (1.0f / totalTime * Time.deltaTime );
            if(seconds == 0)
            {
                player.GetComponent<PlayerController>().enabled = false;
                player.GetComponent<Animator>().enabled = false;
                player.GetComponent<CapsuleCollider>().enabled = false;
                player.GetComponent<SABoneColliderBuilder>().enabled = false;
                enemy.GetComponent<NavMeshAgent>().enabled = false;
                enemy.GetComponent<EnemyNavigationController>().enabled = false;
                //enemy.GetComponent<Animator>().enabled = false;
                Point.SetActive(false);
                Result.SetActive(true);
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
		}
	}
}
