﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public GameObject MainCamera;
    public GameObject TargetObject;
    public GameObject StaminaImage;

    private Vector3 lastMousePosition;
    private Vector3 newAngle = new Vector3(0, 0, 0);

    Rigidbody rigid;
    private Animator motion;

    float Stamina = 100;
    private string boolname;
    public float Speed = 1;
    public float smooth = 100;

    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        motion = GetComponent<Animator>();
        StaminaImage.GetComponent<Image>();
        //MainCamera = transform.Find("Main Camera").gameObject;

        DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
        newAngle = MainCamera.transform.localEulerAngles;
        lastMousePosition = Input.mousePosition;
    }

    void Update()
    {
        Vector3 worldPos = this.transform.position;
        float mypos_y = worldPos.y;
        if(worldPos.y < 0.3) {
            Vector3 pos = new Vector3(0, 2.0f-mypos_y, -3);//tps: (0, 2-mypos_y, -3), fps: (0, 1.1f-mypos_y, 0)
            MainCamera.transform.localPosition = pos;
        }

        //カメラ上下移動
        newAngle.x -= (Input.mousePosition.y - lastMousePosition.y) * 0.03f;
        //newAngle.x -= Input.GetAxis("Vertical") * 0.03f;
        MainCamera.gameObject.transform.localEulerAngles = newAngle;
        lastMousePosition = Input.mousePosition;

        Vector3 target_dir = new Vector3(0, 0, Input.GetAxis("Vertical"));

        //体、カメラの向きを変更
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        transform.RotateAround(transform.position, Vector3.up, Input.GetAxis("Mouse X")*3);
        transform.RotateAround(transform.position, Vector3.up, Input.GetAxis("Horizontal") * 3);

        //前方へ歩く
        if(target_dir.magnitude > 0.1){
            /*if(Input.GetKey(KeyCode.S))
            {
                float rq = -target_dir.z;
                Quaternion rotation = Quaternion.LookRotation(new Vector3(0, 0, rq));
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * smooth);
                target_dir = new Vector3(0, 0, 1);
            } (後ろを向く) */
            transform.Translate(target_dir * Time.deltaTime * Speed);
            //アニメーション用
            motion.SetBool(boolname, true);
        }
        else{
            motion.SetBool(boolname, false);
        }

        //走る
        if(Stamina <= 0.4f){
            Speed = 1;
            boolname = "WalkBool";
            motion.SetBool("RunBool", false);
            Stamina += 0.005f;
        }
        else{
            if((Input.GetKey(KeyCode.LeftShift) || Input.GetButton("Fire3")) && target_dir.magnitude > 0.1 && Stamina >= 0.50f){
                Speed = 5;
                boolname = "RunBool";
                Stamina -= 0.30f;
                StaminaImage.GetComponent<Image>().fillAmount -= 0.003f;
            }
            else{
                Speed = 1;
                boolname = "WalkBool";
                motion.SetBool("RunBool", false);
                if(Stamina <= 99.5){
                    Stamina += 0.20f;
                    StaminaImage.GetComponent<Image>().fillAmount += 0.002f;
                }
            }
        }
    }
}
