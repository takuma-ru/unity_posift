﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{

    //プレイヤーを変数に格納
    public GameObject Target;

    //回転させるスピード
    public float rotateSpeed = 0.1f;

    public static float rot_y;// = 0;
    public static Vector3 pos;// = new Vector3(0, 10, -8);

    // Use this for initialization
    void Start()
    {
        if (rot_y != 0)
        {
            this.transform.eulerAngles = new Vector3(25, rot_y, 0);
            this.transform.position = pos;
        }
        else
        {
            this.transform.eulerAngles = new Vector3(25, 0, 0);
            this.transform.position = new Vector3(0, 10, -8);
        }
        
    }

    // Update is called once per frame
    void Update()
    {

        //回転させる角度
        float angle =  rotateSpeed;

        //プレイヤー位置情報
        Vector3 playerPos = Target.transform.position;

        //カメラを回転させる
        transform.RotateAround(playerPos, Vector3.up, angle);

        CameraController_log.rot_y = this.transform.eulerAngles.y;
        CameraController_log.pos = this.transform.position;
    }

    void Awake()
    {
        Application.targetFrameRate = 60;
        //Debug.Log(CameraController_log.rot_y);
        //Debug.Log(CameraController_log.pos);
    }
}