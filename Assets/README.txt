-注意事項-
実行ファイルは、「Posift_Built」ファルダに入っています。
loginSceneとGameSceneにある、「NCMBSettings」と「NCMBManager」は変更しないでください。正常に動かなくなります。
また、「NCMBSettings」のApplicationKeyとClientKeyはサーバと通信するために必要です。流用しないでください。